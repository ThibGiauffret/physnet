# !/bin/bash

# Find all database.db files in the dist directory
find ./ -name "database.db" -exec rm -rf {} \;
# Remove the shm and wal files
find ./ -name "database.db-shm" -exec rm -rf {} \;
find ./ -name "database.db-wal" -exec rm -rf {} \;
# Copy the database.db.backup file to database.db in the same directory
find ./ -name "database.db.backup" -exec sh -c 'cp "$0" "${0%/*}/database.db"' {} \;