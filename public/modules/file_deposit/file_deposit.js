
$(document).ready(function () {

    // Check if local storage contains username and file
    if (localStorage.getItem("username") != undefined) {
        $("#username").val(localStorage.getItem("username"));
    }

    // Get parameters from URL
    var url_string = window.location.href;
    var url = new URL(url_string);
    file_deposit_id = url.searchParams.get("id");
    message = url.searchParams.get("message");
    console.log("Document activity is : " + file_deposit_id);

    // Set the value of the file_deposit_id input
    $("#file_deposit_id").val(file_deposit_id);

    socket.on("connect", () => {
        console.log(socket.id);
        socket.emit("join_file_deposit", file_deposit_id);
    });

    socket.on("index_file_deposit", (data) => {
        title = data.title;
        instructions = data.instructions;
        size = data.max_size;
        extensions = data.extensions;

        $("#title").html(title);
        $("#instructions").html(instructions);
        $("#size").html(size + " Mo");
        $("#extensions").html(extensions.replace(/,/g, ", "));
    });


switch (message) {
    case "name_error":
        $("#message").html("<div class='alert alert-warning alert-dismissible fade show' role='alert'><i class=\"fa-solid fa-circle-exclamation\"></i>&nbsp;Veuillez renseigner votre nom avant de déposer votre travail&nbsp;!<button type='button' class='close' data-bs-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>");
        $("#username").addClass("is-invalid");
        break;

    case "file_error":
        $("#message").html("<div class='alert alert-warning alert-dismissible fade show' role='alert'><i class=\"fa-solid fa-circle-exclamation\"></i>&nbsp;Veuillez sélectionner un fichier afin de le déposer&nbsp;!<button type='button' class='close' data-bs-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>")
        $("#fileDeposit").addClass("is-invalid");
        break;

    case "password_error":
        $("#message").html("<div class='alert alert-warning alert-dismissible fade show' role='alert'><i class=\"fa-solid fa-circle-exclamation\"></i>&nbsp;Le code de dépôt est erroné&nbsp;!<button type='button' class='close' data-bs-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>")
        $("#password").addClass("is-invalid");
        break;

    case "size_error":
        $("#message").html("<div class='alert alert-warning alert-dismissible fade show' role='alert'><i class=\"fa-solid fa-circle-exclamation\"></i>&nbsp;Votre fichier dépasse la taille maximale autorisée&nbsp;!<button type='button' class='close' data-bs-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>")
        $("#size").addClass("text-danger");
        $("#fileDeposit").addClass("is-invalid");
        break;

    case "extension_error":
        $("#message").html("<div class='alert alert-warning alert-dismissible fade show' role='alert'><i class=\"fa-solid fa-circle-exclamation\"></i>&nbsp;Votre fichier n'a pas une extension autorisée&nbsp;!<button type='button' class='close' data-bs-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>")
        $("#extensions").addClass("text-danger");
        $("#fileDeposit").addClass("is-invalid");
        break;

    case "success":
        $("#message").html("<div class='alert alert-success alert-dismissible fade show' role='alert'><i class=\"fa-solid fa-circle-check\"></i>&nbsp;Votre fichier a bien été déposé&nbsp;!<button type='button' class='close' data-bs-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>");
        clearForm();
        break;

    case "error":
        $("#message").html("<div class='alert alert-danger alert-dismissible fade show' role='alert'><i class=\"fa-solid fa-circle-xmark\"></i>&nbsp;Une erreur est survenue lors du dépôt de votre fichier. Veuillez contacter votre professeur&nbsp;!<button type='button' class='close' data-bs-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>")
        break;
}
})

function clearForm() {
    $("#username").removeClass("is-invalid");
    $("#file").removeClass("is-invalid");
    $("#size").removeClass("text-danger");
    $("#extensions").removeClass("text-danger");
    // Reset form
    $("#username").val("");
    $("#file").val("");
    $("#password").val("");
    // Remove from local storage
    localStorage.removeItem("username");
    localStorage.removeItem("file");
}

function saveForm(){
    // Save form to local storage
    localStorage.setItem("username", $("#username").val());
}
