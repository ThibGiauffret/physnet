# Generate 1000 random names for artillery testing and save them to a file random_names.csv

import random
import string

def random_name():
    length = random.randint(5, 10)
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(length))

def main():
    names = []
    for i in range(1000):
        names.append(random_name())
    with open('random_names.csv', 'w') as f:
        for name in names:
            f.write(name + '\n')
    f.close()
    
if __name__ == '__main__':
    main()
    
