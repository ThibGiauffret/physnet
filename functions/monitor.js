const os = require('os');
const fs = require('fs');
const process = require('process');
const { get } = require('http');
const getSize = require('get-folder-size');
let dataFolderSize = 0;
var databaseSize = 0;

var connect = (io, socket, os, fs, path, log, modules) => {

    var load = os.loadavg()[0];
    var totalMemory = os.totalmem();
    var freeMemory = os.freemem();
    var usedMemory = Number((totalMemory - freeMemory) / 1073741824).toFixed(4);
    

    // Memory used by node js app
    const formatMemoryUsage = (data) => `${Math.round(data / 1024 / 1024 * 100) / 100} Mo`;

    var memoryData = process.memoryUsage();
    var nodeMemoryUsage = formatMemoryUsage(memoryData.rss);

    socket.on('get_resources', function () {
        // log("Updating resources on admin panel")
        load = os.loadavg()[0];
        freeMemory = os.freemem();
        usedMemory = Number((totalMemory - freeMemory) / 1073741824).toFixed(4);
        memoryData = process.memoryUsage();
        nodeMemoryUsage = formatMemoryUsage(memoryData.rss);
        io.to("admin").emit('resources', { cpu: load, memory: usedMemory, nodeMemory: nodeMemoryUsage, databaseSize: Number(databaseSize).toFixed(3), dataFolderSize: Number(dataFolderSize).toFixed(3) });

    });

    // Get the size of the databases
    socket.on('get_database_size', function () {
        log("Calculating database size")
        databaseSize = 0;
        // Get the size of the main database
        getDatabaseSize(path + "/database.db").then((size) => {

            databaseSize += size;
            // For each module, get the size of the database
            getModulesDatabaseSize().then((data) => {
                databaseSize += data.size;
                data.message = data.message.substring(0, data.message.length - 1);
                log("Total database size : " + databaseSize.toFixed(3) + " Mo\n -- Main database : " + size +"\n"+ data.message);
                io.to("admin").emit('resources', { cpu: load, memory: usedMemory, nodeMemory: nodeMemoryUsage, databaseSize: Number(databaseSize).toFixed(3), dataFolderSize: Number(dataFolderSize).toFixed(3) });
            }).catch((err) => {
                log("Error with modules database : " + err);
            })
        })
    });

    function getDatabaseSize(location) {
        // Promise to get the size of the database.db file
        return new Promise(function (resolve, reject) {
            var theDatabaseSize = 0;
            fs.stat(location, (error, stats) => {
                // in case of any error
                if (error) {
                    reject(error);
                    return;
                }
                // Get file size in bytes
                theDatabaseSize = stats.size / 1000000.0;
                resolve(theDatabaseSize);
            });
        });
        
    }

    function getModulesDatabaseSize() {
        return new Promise((resolve, reject) => {
            var modulesDatabaseSize = 0;
            let counter = 0;
            let full_message = "";
            modules.forEach((module) => {
                getDatabaseSize(path + "/db/modules/" + module.id + "/database.db").then((size) => {
                    counter++;
                    full_message += " -- "+module.id + " : " + size.toFixed(3) + " Mo\n";
                    modulesDatabaseSize += size;
                    if (counter === modules.length) {
                        resolve({ size: modulesDatabaseSize, message: full_message });
                    }
                }).catch((err) => {
                    log("Error with module " + module.id + " database : " + err);
                })
            })
        })
    }


    // Get the size of the data folder
    socket.on('get_data_folder_size', function () {
        log("Calculating data folder size")
        dataFolderSize = 0;
        getDirSize(path + "/data/shared_files/").then((size) => {

            log("Shared files folder size  : " + size.toFixed(3) + " Mo")
            dataFolderSize += size;
            // For each module, get the size of the folder
            getModulesDirSize().then((data) => {
                data.message = data.message.substring(0, data.message.length - 1);
                log("Assignments folder size : " + data.size.toFixed(3) + " Mo\n" + data.message);
                dataFolderSize += data.size;
                io.to("admin").emit('resources', { cpu: load, memory: usedMemory, nodeMemory: nodeMemoryUsage, databaseSize: Number(databaseSize).toFixed(3), dataFolderSize: Number(dataFolderSize).toFixed(3) });
            }).catch((err) => {
                log("Error getting data folder sizes : " + err);
            })
        });
    })

    function getModulesDirSize() {
        return new Promise((resolve, reject) => {
            var folderSize = 0;
            let counter = 0;
            let full_message = "";
            modules.forEach((module) => {
                getModuleDirSize(module.id).then((data) => {
                    counter++;
                    full_message += " -- "+data.message + "\n";
                    folderSize += data.size;
                    if (counter === modules.length) {
                        resolve({ size: folderSize, message: full_message });
                    }
                }).catch((err) => {
                    log("Error getting module " + module.id + " data folder size : " + err);
                })
            })
        })
    }

    function getModuleDirSize(moduleId) {
        return new Promise((resolve, reject) => {
            getDirSize(path + "/data/assignments/" + moduleId).then((size) => {
                let message = moduleId + " : " + size.toFixed(3) + " Mo, ";
                resolve({ size: size, message: message });
            }).catch((err) => {
                reject(err);
            })
        })
    }

    function getDirSize(folder) {
        // Promise to get the size of a folder
        return new Promise(function (resolve, reject) {
            getSize(folder, (err, size) => {
                if (err) { reject(err); }
                resolve(size / 1024 / 1024);
            });
        });

        
    }
};

module.exports.connect = connect;