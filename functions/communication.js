// Module to handle classrooms loading
module.exports = function (io, socket, root, fs, log) {

  const pool = require("./db_pool_config").getPool();

  // Get the number of connected users in all rooms
  socket.on("get_connected_users", function () {
    var connected_users = 0;
    for (var room of io.sockets.adapter.rooms.keys()) {
      // if the room is a classroom or main
      if (room.startsWith("classroom_") || room == "main" || room.startsWith("quiz_")) {
        connected_users += io.sockets.adapter.rooms.get(room).size;
      }
    }
    socket.emit("connected_users", connected_users);
  });

  function addMessageToDatabase(data) {
    return new Promise((resolve, reject) => {
      pool.acquire().then((db) => {
        var stmt = db.prepare("INSERT INTO classroom_messages (classroom, date, type, content) VALUES (?, ?, ?, ?)");
        stmt.run(data.classroom, data.date, data.type, data.content, function (err) {
          if (err) {
            reject(err);
          } else {
            resolve();
          }
        });
        stmt.finalize();
        pool.release(db);
      })
    })
  }

  // Get the message from admin and send it to the client
  socket.on("emit_com_link", function (data) {
    io.to("classroom_" + data.classroom).emit("com_link", { url: data.url, mode: data.mode });
    let thedate = new Date();
    // Store the message in database
    addMessageToDatabase({ classroom: data.classroom, date: thedate, type: "link", content: data.url }).then(() => {
      log("Message stored in database");
    }).catch((err) => {
      log("Error storing message in database");
      log(err);
    })
  });

  // Get the message from admin and send it to the client
  socket.on("emit_com_message", function (data) {
    io.to("classroom_" + data.classroom).emit("com_message", data.message);
    let thedate = new Date();
    // Store the message in database
    addMessageToDatabase({ classroom: data.classroom, date: thedate, type: "message", content: data.message }).then(() => {
      log("Message stored in database");
    }).catch((err) => {
      log("Error storing message in database");
      log(err);
    })
  });

  // Get the file from admin and send it to the client
  socket.on("emit_com_file", function (data, file, callback) {
    // Check if the file already exists
    if (fs.existsSync(root + "/data/shared_files/" + data.name) && data.overwrite == false) {
      return callback({ message: "exists" });
    }
    // save the content to the disk, for example
    fs.writeFile(root + "/data/shared_files/" + data.name, file, (err) => {
      callback({ message: err ? "failure" : "success" });
      if (err) {
        log(err);
      } else {
        // Get file url
        url = data.name;
        io.to("classroom_" + data.classroom).emit("com_file", { url: url });
        let thedate = new Date();
        // Store the message in database
        addMessageToDatabase({ classroom: data.classroom, date: thedate, type: "file", content: url }).then(() => {
          log("Message stored in database");
        }).catch((err) => {
          log("Error storing message in database");
          log(err);
        })
      }
    });
  });


  // Load previous notifications
  socket.on("load_notif", function (classroom) {
    loadNotif(classroom);
  });

  function loadNotif(classroom){
    // Get the messages from database
    pool.acquire().then((db) => {
      var stmt = db.prepare("SELECT * FROM classroom_messages WHERE classroom = ? ORDER BY date ASC").all(classroom, function (err, res) {
        if (err) {
          log(err);
        }
        if (res == []) {
          log("No message found");
        } else {
          io.to("classroom_" + classroom).emit("load_notif2", { data: res });
        }
      });
      stmt.finalize();
      pool.release(db);
    })
  }

  // Load the list of classrooms
  socket.on("select_classroom_messages", function (data) {
    // Get the messages from database
    pool.acquire().then((db) => {
      var stmt = db.prepare("SELECT * FROM classroom_messages WHERE classroom = ? ORDER BY date DESC").all(data, function (err, res) {
        if (err) {
          log(err);
        }
          io.to("admin").emit("classroom_messages", { classroom: data, messages: res });
      });
      stmt.finalize();
      pool.release(db);
    });
  })
  

  socket.on("delete_classroom_message", function (data, callback) {
    if (data.type == "file") {
      // Find the message with the type "file" and remove it from disk
      pool.acquire().then((db) => {
        var stmt = db.prepare("SELECT * FROM classroom_messages WHERE id = ?").get(data.index, function (err, res) {
          if (err) {
            log(err);
            callback({ message: "failure" });
          }
          console.log(res);
          fs.unlink(root + "/data/shared_files/" + res.content, (err) => {
            if (err) {
              log(err);
              callback({ message: "failure" });
            }
          });
          removeFileMessage(data).then(() => {
            log("Message removed from database")
            callback({ message: "success" });
          }).catch((err) => {
            log(err);
            callback({ message: "failure" });
          })
        });
        stmt.finalize();
        pool.release(db);
      });
    } else {
      removeFileMessage(data).then(() => {
        log("Message removed from database")
        callback({ message: "success" });
      }).catch((err) => {
        log(err);
        callback({ message: "failure" });
      })
    }
  });

  function removeFileMessage(data){
    return new Promise((resolve, reject) => {
    // Remove the message from database
    pool.acquire().then((db) => {
      var stmt = db.prepare("DELETE FROM classroom_messages WHERE id = ?");
      stmt.run(data.index, function (err) {
        if (err) {
          reject({ message: "failure" });
        }
        loadNotif(data.classroom);
        resolve({ message: "success" });
      });
      stmt.finalize();
      pool.release(db);
    });
  });
  }

  socket.on("delete_classroom_messages", function (data, callback) {
    // Find all classroom messages with the type "file" and remove them from disk
    pool.acquire().then((db) => {
      var stmt = db.prepare("SELECT * FROM classroom_messages WHERE classroom = ? AND type = 'file'").all(data, function (err, res) {
        if (err) {
          log(err);
          return callback({ message: "failure" });
        }
        for (var file of res) {
          fs.unlink(root + "/data/shared_files/" + file.content, (err) => {
            if (err) {
              log(err);
              return callback({ message: "failure" });
            }
          });
        }

        removeAllFilesMessage(data).then(() => {
          log("All messages removed from database")
          return callback({ message: "success" });
        }).catch((err) => {
          log(err);
          return callback({ message: "failure" });
        })
      });
      stmt.finalize();
      pool.release(db);
    });
    
  });

  function removeAllFilesMessage(data){
    return new Promise((resolve, reject) => {
    // Remove all messages from database for a specific classroom
    pool.acquire().then((db) => {
      var stmt = db.prepare("DELETE FROM classroom_messages WHERE classroom = ?");
      stmt.run(data, function (err) {
        if (err) {
          reject({ message: "failure" });
        }
        loadNotif(data);
        resolve({ message: "success" });
      });
      stmt.finalize();
      pool.release(db);
    });
  });
  }

}