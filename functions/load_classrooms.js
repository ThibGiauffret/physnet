// Module to handle classrooms loading
module.exports = function (io, socket, log) {

  const pool = require("./db_pool_config").getPool();

  // Send the classrooms to the client (main page)
  socket.on("load_classrooms", function () {
    loadClassrooms().then((classrooms) => {
      socket.emit("load_classrooms", classrooms);
    });
  });

  socket.on("load_classroom_list", function () {
    loadClassroomList().then((classrooms) => {
      socket.emit("load_classroom_list", classrooms);
    }).catch((err) => {
      log("Error loading classroom list");
      log(err);
    });
  });

  function loadClassroomList() {
    return new Promise(function (resolve, reject) {
      pool.acquire().then((db) => {

        var stmt = db.prepare("SELECT id, title, active, visible FROM classrooms_base ORDER BY id DESC").all(function (err, res) {
          if (err) {
            reject(err);
          }
          if (res == []) {
            reject("No classroom found");
          } else {
            resolve(res);
          }
        });
        stmt.finalize();

        pool.release(db);
      });
    });

  }

  // Send the levels to the client (main page)
  socket.on("load_levels", function () {
    loadLevels().then((levels) => {
      socket.emit("load_levels", levels);
    })
  });

  // Refresh the classrooms 
  socket.on("refresh_classroom", function (data) {
    io.to("classroom_" + data.id).emit("refresh_classroom");
    loadLevels().then((levels) => {
      io.to("main").emit("load_levels", levels);
    });
  });

  // Send the classroom to the client (classroom page)
  socket.on("load_classroom", function (classroom_id) {
    // Check if classroom id is a number
    if (isNaN(classroom_id)) {
      socket.emit("redirect", "/index?message=danger&content=La classe n'existe pas !");
      return;
    }
    loadClassroom(classroom_id, false).then((classroom) => {
      if (classroom == "classroom_not_found") {
        socket.emit("redirect", "/index?message=danger&content=La classe n'existe pas !");
      } else if (classroom == "load_classroom_pass") {
        socket.emit("load_classroom_pass");
      } else {
        socket.emit("load_classroom", classroom);
        socket.join("classroom_" + classroom_id);
      }
    })
  });

  // Handle the classroom password
  socket.on("unlock_classroom", function (data) {
    pass = data.pass;
    classroom_id = data.classroom;
    // Check if classroom id is a number
    if (isNaN(classroom_id)) {
      socket.emit("error_classroom_password", "classroom_id_not_number");
      return;
    }
    // Check if pass is only composed of numbers and letters
    if (!/^[a-zA-Z0-9]+$/.test(pass)) {
      socket.emit("error_classroom_password", "wrong_password");
      return;
    }
    unlockClassroom(classroom_id, pass).then((result) => {
      if (result == "pass_ok") {
        loadClassroom(classroom_id, true).then((classroom) => {
          socket.emit("load_classroom", classroom);
          socket.join("classroom_" + classroom_id);

        })
      } else {
        socket.emit("error_classroom_password", "wrong_password");
      }
    })
  })

  socket.on("leave_classroom", function (classroom) {
    // Check if classroom id is a number
    if (!isNaN(classroom)) {
      socket.leave("classroom_" + classroom);
      log("Le client " + socket.id + " a quitté la salle de classe : " + classroom)
    }
  });

  function unlockClassroom(classroom, pass) {
    return new Promise(function (resolve, reject) {
      pool.acquire().then((db) => {
        var stmt = db.prepare("SELECT pass FROM classrooms_base WHERE id =  ?").get(classroom, function (err, res) {
          if (err) {
            reject(err);
          }
          if (res == undefined) {
            reject("classroom_not_found");
          } else {
            if (res.pass == pass) {
              resolve("pass_ok")
            } else {
              resolve("wrong_pass")
            }
          }
        })
        stmt.finalize();
        pool.release(db);
      });
    });
  }


  // Load a specific classroom from database
  function loadClassroom(classroom, unclock) {
    return new Promise(function (resolve, reject) {
      pool.acquire().then((db) => {

        var stmt = db.prepare("SELECT id, title, level, date, instructions, documents, time, pass, active, visible FROM classrooms_base WHERE id = ?").get(classroom, function (err, res) {
          if (err) {
            reject(err);
          }
          if (res == undefined) {
            resolve("classroom_not_found")
          } else {
            if (res.active == 0) {
              resolve("classroom_not_found")
            } else {
              if ((res.pass != "" && res.pass != null) && unclock == false) {
                resolve("load_classroom_pass")
              } else {
                loadDocuments(res.documents).then(function (documents) {
                  res.documents = documents;
                  // Remove pass from res
                  delete res.pass;
                  resolve(res)
                }).catch(function (err) {
                  reject(err)
                })
              }
            }
          }
        });
        stmt.finalize();
        pool.release(db);
      });
    })
  }

  // Load classrooms from database
  function loadClassrooms() {
    return new Promise(function (resolve, reject) {
      pool.acquire().then((db) => {

        var stmt = db.prepare("SELECT id, title, level, date, active, visible FROM classrooms_base WHERE visible=1 ORDER BY date ASC").all(function (err, res) {
          if (err) {
            reject(err);
          }
          if (res == []) {
            reject("No classroom found");
          } else {
            resolve(res);
          }
        })
        stmt.finalize();
        pool.release(db);
      });
    });

  }

  // Load levels from database
  function loadLevels() {
    return new Promise(function (resolve, reject) {
      pool.acquire().then((db) => {
        let levels = [];
        // Select distinct levels from classrooms_base
        var stmt = db.prepare("SELECT DISTINCT level FROM classrooms_base WHERE visible=1 ORDER BY level DESC").all(function (err, res) {
          if (err) {
            reject(err);
          }
          if (res == []) {
            reject("Error loading levels");
          } else {
            for (var level of res) {
              levels.push(level.level);
            }
            resolve(levels);
          }
        });
        stmt.finalize();
        pool.release(db);
      });

    });
  }


  // Load documents for a specific classroom from database
  function loadDocuments(documents) {
    // promise to load documents
    return new Promise(function (resolve, reject) {
      pool.acquire().then((db) => {
        document_list = JSON.parse(documents)
        documents = document_list.join(",");

        var stmt = db.prepare(`SELECT id, type, icon, title, text, url, external FROM classrooms_documents WHERE id IN (${documents})`).all(function (err, res) {
          if (err) {
            reject(err);
          }
          if (res == []) {
            reject("No document found");
          } else {
            // Order the results by the order of the documents
            res.sort(function (a, b) {
              return document_list.indexOf(a.id) - document_list.indexOf(b.id);
            });
            resolve(res);
          }
        });
        stmt.finalize();
      });
    })

  }

};
