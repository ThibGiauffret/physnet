exports.load = function (io, socket, path, log) {
    var db_path = path + '/db/modules/quiz/database.db';
    require('./quiz.js')(socket, path, db_path, log);
    require('./add_quiz.js')(io, socket, path, db_path, log);
    require('./modify_quiz.js')(io, socket, path, db_path, log);
};

// Export unauthorised variable
exports.unauthorized = [
    'get_last_quiz_ids',
    'create_quiz',
    "load_quiz_list",
    'modify_quiz',
    'select_quiz',
    'delete_quiz',
    'get_results_list',
    'delete_results',
]

// Module name
exports.name = 'quiz';

// Module id
exports.id = 'quiz'; // used in addClassroom and modifyClassroom