module.exports.modifyInDB = function (path, data, log) {
    const pool = require(path + '/functions/db_pool_config.js').getPool(path + '/database.db');
    return new Promise((resolve, reject) => {

        pool.acquire().then((db) => {
            // Check if file deposit already exists
            if (data.update == true) {
                var stmt = db.prepare("UPDATE classrooms_documents SET type = ?, icon = ?, title = ?, text = ?, url = ?, external = ? WHERE id = ?").run([data.type, "fa-brands fa-python", "Cahier Python", data.text, "./notebook?id=" + data.id, 0, data.doc_id], function (err) {
                    if (err) {
                        reject(err);
                    }
                    if (this.changes == 0) {
                        reject("Notebook update failed");
                    } else {
                        log("Updated notebook row");
                        resolve(data.doc_id);
                    }
                });
                stmt.finalize();
            } else {
                var stmt = db.prepare("INSERT INTO classrooms_documents (type, icon, title, text, url, external) VALUES (?, ?, ?, ?, ?, ?)").run([data.type, "fa-brands fa-python", "Cahier Python", data.text, "./notebook?id=" + data.id, 0], function (err) {
                    if (err) {
                        reject(err);
                    }
                    if (this.changes == 0) {
                        reject("Notebook insertion failed");
                    } else {
                        log("Added notebook row : " + this.lastID);
                        resolve(this.lastID);
                    }
                });
                stmt.finalize();
            }
            pool.release(db);
        });
    })
}