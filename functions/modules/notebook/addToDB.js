module.exports.addToDB = function (path, data, log) {
    const pool = require(path + '/functions/db_pool_config.js').getPool(path + '/database.db');
    return new Promise((resolve, reject) => {
        pool.acquire().then((db) => {
            var stmt = db.prepare("INSERT INTO classrooms_documents VALUES (NULL,?,?,?,?,?,?)").run([data.type, "fa-brands fa-python", "Cahier Python", data.text, "./notebook?id=" + data.id, 0], function (err) {
                if (err) {
                    reject(err);
                }
                if (this.changes == 0) {
                    reject("Notebook insertion failed");
                } else {
                    log("Inserted notebook row : " + this.lastID);
                    resolve(this.lastID);
                }
            });
            stmt.finalize();
            pool.release(db);
        })
    })
}