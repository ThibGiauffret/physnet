module.exports = function (app,express,path) {
    app.use(express.static(path + "/public/modules/audio_record/"));
    app.get('/audio', (req, res) => {
        res.sendFile(path + '/public/modules/audio_record/audio_record.html')
    })
}