// Module to handle audio
module.exports = function (io, socket, path, db_path, fs, log) {

    const pool = require(path + '/functions/db_pool_config.js').getPool(db_path);

    // Send index of audio files
    socket.on('join_audio', (id) => {
        // Check if id is a number
        if (isNaN(id)) {
            // Redirect to home page
            socket.emit("redirect", "/index?message=danger&content=L'oral demandé n'existe pas.");
            return;
        }
        getAudioIndex(id).then((data) => {
            if (data == "audio_not_found") {
                socket.emit("redirect", "/index?message=danger&content=L'oral demandé n'existe pas.");
                return;
            }
            socket.emit('index_audio', data);
        }).catch((err) => {
            log(err);
            socket.emit("redirect", "/index?message=danger&content=Une erreur est survenue lors du chargement du module d'oral. Veuillez contacter votre professeur.");
        })
    });

    function getAudioIndex(id) {
        return new Promise((resolve, reject) => {
            pool.acquire().then((db) => {
                var stmt = db.prepare("SELECT * FROM audio_base WHERE id = ?").get(id, function (err, res) {
                    if (err) {
                        reject(err);
                    }
                    if (res == undefined) {
                        resolve("audio_not_found");
                    } else {
                        resolve(res);
                        // Remove pass from res
                        delete res.pass;
                    }
                });
                stmt.finalize();
                pool.release(db);
            });
        })
    }

    function checkAudioPassword(id) {
        return new Promise((resolve, reject) => {
            pool.acquire().then((db) => {

                var stmt = db.prepare("SELECT pass FROM audio_base WHERE id = ?").get(id, function (err, res) {
                    if (err) {
                        reject(err);
                    }
                    if (res == undefined) {
                        reject("No audio with this id : " + id);
                    } else {
                        resolve(res.pass);
                    }
                });
                stmt.finalize();
                pool.release(db);
            });
        })
    }

    // Receive audio 
    socket.on('audio', (data, callback) => {
        filename = data.filename;
        user_pass = data.pass;

        checkAudioPassword(data.id).then((pass) => {
            if (pass == user_pass) {

                // Check if file exists
                if (fs.existsSync(path + '/data/assignments/audio_record/' + filename)) {
                    // File exists
                    callback("exists");
                } else {
                    audio = data.file;
                    // Buffer audio
                    audio = Buffer.from(audio, 'base64');

                    // Save audio to file
                    fs.writeFile(path + '/data/assignments/audio_record/' + filename, audio, function (err) {
                        if (err) {
                            log(err);
                            callback("error");
                        } else {
                            callback("success");
                        }
                    }
                    );
                }
            } else {
                callback("wrong_pass");
            }
        });
    });

    socket.on("create_audio", (data, callback) => {
        time = data.time;
        // Convert time to seconds
        time = time.split(":");
        time = parseInt(time[0]) * 60 + parseInt(time[1]);

        pool.acquire().then((db) => {
            var stmt = db.prepare("INSERT INTO audio_base (title, instructions, time, pass) VALUES (?, ?, ?, ?)").run([data.title, data.instructions, time, data.pass], function (err) {
                if (err) {
                    log(err);
                    callback("error");
                }
                if (this.changes == 0) {
                    log("Audio adding failed");
                    callback("error");
                } else {
                    callback("success");
                }
            });
            stmt.finalize();
            pool.release(db);
        });
    })

    socket.on("load_audio_list", () => {
        loadAudioList().then((rows) => {
            io.to("admin").emit("load_audio_list", rows);
        }).catch((err) => {
            log(err);
        })
    })

    function loadAudioList() {
        return new Promise((resolve, reject) => {
            pool.acquire().then((db) => {
                var stmt = db.prepare("SELECT id, title FROM audio_base ORDER BY id DESC").all(function (err, res) {
                    if (err) {
                        reject(err);
                    }
                    if (res.length == 0) {
                        reject("No audio in database");
                    } else {
                        resolve(res);
                    }
                });
                stmt.finalize();
                pool.release(db);
            });
        })
    }

    socket.on("select_audio", (id, callback) => {
        selectAudio(id).then((data) => {
            callback(data);
        }).catch((err) => {
            log(err);
        })
    })

    function selectAudio(id) {
        return new Promise((resolve, reject) => {
            pool.acquire().then((db) => {
                var stmt = db.prepare("SELECT * FROM audio_base WHERE id = ?").get(id, function (err, res) {
                    if (err) {
                        reject(err);
                    }
                    if (res == undefined) {
                        reject("No audio with this id : " + id);
                    } else {
                        resolve(res);
                    }
                });
                stmt.finalize();
                pool.release(db);
            });
        })
    }

    socket.on("modify_audio", (data, callback) => {
        time = data.time;
        // Convert time to seconds
        time = time.split(":");
        time = parseInt(time[0]) * 60 + parseInt(time[1]);

        modifyAudio(data, time).then((data) => {
            callback(data);
        }).catch((err) => {
            log(err);
        })
    })

    function modifyAudio(data, time) {
        return new Promise((resolve, reject) => {
            pool.acquire().then((db) => {
                var stmt = db.prepare("UPDATE audio_base SET title = ?, instructions = ?, time = ?, pass = ? WHERE id = ?").run([data.title, data.instructions, time, data.pass, data.id], function (err) {
                    if (err) {
                        reject(err);
                    }
                    if (this.changes == 0) {
                        reject("Error while updating audio row");
                    } else {
                        log("Updated audio row : " + data.id);
                        resolve("success");
                    }
                });
                stmt.finalize();
                pool.release(db);
            })
        });
    }

    socket.on("delete_audio", (id, callback) => {
        log("Deleting audio : " + id);
        deleteAudio(id).then((data) => {
            callback(data);
        }).catch((err) => {
            log(err);
            callback("error");
        })
    })

    function deleteAudio(id) {
        return new Promise((resolve, reject) => {
            pool.acquire().then((db) => {
                var stmt = db.prepare("DELETE FROM audio_base WHERE id = ?").run(id, function (err) {
                    if (err) {
                        reject(err);
                    }
                    if (this.changes == 0) {
                        reject("Error while deleting audio row");
                    } else {
                        log("Deleted audio row : " + id);
                        resolve("success");
                    }
                });
                stmt.finalize();
                pool.release(db);
            })
        });
    }

    socket.on("get_audio_files", (id) => {
        // With fs, go to /data/assignments/audio_record and list all files
        getAudioFiles(id).then((files) => {
            io.to("admin").emit("audio_files_admin", files);
        })
    })

    function getAudioFiles(id) {
        return new Promise((resolve, reject) => {
            fs.readdir(path + '/data/assignments/audio_record', function (err, files) {
                if (err) {
                    log(err);
                    reject(err);
                } else {
                    files_list = [];
                    // Filter files witch start with "audio_"+id
                    files = files.filter(file => file.startsWith("audio_" + id));
                    for (var i = 0; i < files.length; i++) {
                        file = {}
                        // Get name between "name_" and next "-"
                        file.name = files[i].split("name_")[1].split("-")[0];
                        // Get date between "date_" and next "."
                        file.date = files[i].split("date_")[1].split(".")[0];
                        // Get size
                        file.size = fs.statSync(path + '/data/assignments/audio_record/' + files[i]).size;
                        // Convert size to Mo
                        file.size = Math.round(file.size / 1000000 * 100) / 100;
                        // Get path
                        file.path = files[i];
                        files_list.push(file);
                    }
                    resolve(files_list);
                }
            })
        });
    }

    socket.on("delete_audio_file", (file_name, callback) => {
        file_path = '/data/assignments/audio_record/' + file_name;
        deleteFile(file_path).then((data) => {
            callback(data);
        })
    })

    function deleteFile(file_path) {
        return new Promise((resolve, reject) => {
            // check if file exists
            fs.exists(path + file_path, function (exists) {
                if (exists) {
                    // Delete file
                    fs.unlink(path + file_path, function (err) {
                        if (err) {
                            log("Error deleting file : " + file_path);
                            log(err);
                            reject(err);
                        } else {
                            resolve("success");
                        }
                    })
                } else {
                    resolve("not_exists");
                }
            })
        });
    }

    socket.on("delete_audio_files", (files, callback) => {
        errors = [];
        for (var i = 0; i < files.length; i++) {
            deleteFile("/data/assignments/audio_record/" + files[i]).then((data) => {
                if (data != "success") {
                    errors.push(data);
                }
            }
            )
        }
        if (errors.length == 0) {
            log("All audio files deleted : " + JSON.stringify(files));
            callback("success");
        }
    })

}