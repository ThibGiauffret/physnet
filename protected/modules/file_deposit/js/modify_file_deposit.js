let fileDepositModifyModal

$(document).ready(function () {

    // Instanciate the classroomModifyModal
    fileDepositModifyModal = new bootstrap.Modal(document.getElementById("fileModifyModal"), {});

})

function selectFileDeposit(id) {
    if (id == "") {
        return;
    }
    console.log(id);
    $("#modify_file_deposit").show();

    $("#modifyFileDepositSubmit").prop("disabled", false);
    $("#deleteFileDeposit").prop("disabled", false);

    socket.emit('select_file_deposit', id, data => {
        $("#file_deposit_mod_id").val(data.id);
        $("#file_deposit_mod_title").val(data.title);
        $("#file_deposit_mod_instructions").summernote('code', data.instructions);
        $("#file_deposit_mod_size").val(data.max_size);
        $("#file_deposit_mod_extensions").val(data.extensions);
        $("#file_deposit_mod_pass").val(data.pass);
    })
}


function modifyFileDeposit() {
    var id = $("#file_deposit_mod_id").val();
    var title = $("#file_deposit_mod_title").val();
    var instructions = $("#file_deposit_mod_instructions").summernote('code');
    var max_size = $("#file_deposit_mod_size").val();
    var extensions = $("#file_deposit_mod_extensions").val();
    var pass = $("#file_deposit_mod_pass").val();

    var errors = []
    if (title == "") {
        $("#file_deposit_title").addClass("is-invalid");
        errors.push("le titre ne peut pas être vide");
    } else if (instructions == "" || instructions == "<p><br></p>" || instructions == "<br>") {
        $("#file_deposit_instructions").addClass("is-invalid");
        errors.push("les consignes ne peuvent pas être vides");
    } else if (pass == "") {
        $("#file_deposit_pass").addClass("is-invalid");
        errors.push("le mot de passe ne peut pas être vide");
    }

    if (errors.length > 0) {
        $("#message_mod_file_deposit").html("<div class='alert alert-danger' role='alert'>Une erreur est survenue lors de la modification de la boîte de dépôt : " + errors.join(", ") + ".</div>");
        return;
    }
    socket.emit('modify_file_deposit', { id: id, title: title, instructions: instructions, max_size:max_size, extensions:extensions, pass: pass }, callback => {
        if (callback == "success") {
            $("#message").html("<div class='alert alert-success' role='alert'>Boîte de dépôt modifiée avec succès.</div>");
            clearModifyFileDeposit();
            // Load the file_deposit list to the select
            socket.emit("load_file_deposit_list");
        } else {
            $("#message_add_file_deposit").html("<div class='alert alert-danger' role='alert'>Une erreur est survenue lors de la modification de la boîte de dépôt.</div>");
        }
    })
}

function clearModifyFileDeposit() {
    $("#file_deposit_mod_title").val("");
    $("#file_deposit_mod_instructions").summernote('code', '');
    $("#file_deposit_mod_size").val("");
    $("#file_deposit_mod_extensions").val("");
    $("#file_deposit_mod_pass").val("");
    $("#message_mod_file_deposit").html("");
    $("#file_deposit_mod_title").removeClass("is-invalid");
    $("#file_deposit_mod_instructions").removeClass("is-invalid");
    $("file_deposit_mod_pass").removeClass("is-invalid");
    $("#modify_file_deposit").hide();
    $("#file_deposit_list_modify").val("").change();
    $("#modifyFileDepositSubmit").prop("disabled", true);
    $("#deleteFileDeposit").prop("disabled", true);
    fileDepositModifyModal.hide();
}

function deleteFileDeposit() {
    if (confirm("Êtes-vous sûr de vouloir supprimer cette boîte de dépôt ?")) {
        var id = $("#file_deposit_mod_id").val();
        socket.emit('delete_file_deposit', id, callback => {
            if (callback == "success") {
                $("#message").html("<div class='alert alert-success' role='alert'>Boîte de dépôt supprimée avec succès.</div>");
                clearModifyFileDeposit();
                // Load the file_deposit list to the select
                socket.emit("load_file_deposit_list");
            } else {
                console.log(callback);
                $("#message").html("<div class='alert alert-danger' role='alert'>Une erreur est survenue lors de la suppression de la boîte de dépôt.</div>");
            }
        })
    }
}