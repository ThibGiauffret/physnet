let notebookModal;

$(document).ready(function () {

    // Instanciate the quizModal
    notebookModal = new bootstrap.Modal(document.getElementById("notebookModal"), {});

})

function createNotebook(){
    var title = $("#notebook_title").val();
    var instructions = $("#notebook_instructions").summernote('code');
    if ($("#notebook_file")[0].files.length != 0){
        var notebook_file = $("#notebook_file")[0].files[0];
    } else {
        var notebook_file = ""
    }
    if ($("#notebook_aux")[0].files.length != 0){
        var notebook_aux = $("#notebook_aux")[0].files[0];
        var notebook_aux_name = notebook_file.name;
    } else {
        var notebook_aux = ""
        var notebook_aux_name = ""
    }
    var doc_url = $("#doc_url").val();
    if ($("#doc_file")[0].files.length != 0){
    var doc_file = $("#doc_file")[0].files[0];
    var doc_file_ext = doc_file.name.split('.').pop();
    } else {
        var doc_file = ""
    }
    var pass = $("#notebook_pass").val();
    var errors = []

    if (title == "" ){
        $("#notebook_title").addClass("is-invalid");
        errors.push("le titre ne peut pas être vide");
    }
    if (instructions == "" || instructions == "<p><br></p>" || instructions == "<br>"){
        $("#notebook_instructions").addClass("is-invalid");
        errors.push("les consignes ne peuvent pas être vides");
    } 
    if (notebook_file == ""){
        $("#notebook_file").addClass("is-invalid");
        errors.push("le fichier du cahier Python ne peut pas être vide");
    }
    if (doc_url != "" && doc_file != ""){
        errors.push("seulement un lien ou un fichier peut être ajouté");
        $("#doc_url").addClass("is-invalid");
        $("#doc_file").addClass("is-invalid");
  
    }if (pass == ""){
        $("#notebook_pass").addClass("is-invalid");
        errors.push("le mot de passe ne peut pas être vide");
    }

    if (errors.length > 0){
        $("#message_add_notebook").html("<div class='alert alert-danger' role='alert'>Une erreur est survenue lors de la création du cahier Python : " + errors.join(", ") + ".</div>");
        return;
    }

    let blocalgo = 0;
    let blocalgo_check = $("#blocalgo_type").prop("checked");
    if (blocalgo_check) {
        blocalgo = 1;
    } else {
        blocalgo = 0;
    }

    socket.emit('create_notebook', {title: title, instructions: instructions, notebook_file: notebook_file, notebook_aux: notebook_aux, notebook_aux_name: notebook_aux_name, doc_url: doc_url, doc_file: doc_file, doc_file_ext: doc_file_ext, blocalgo: blocalgo, pass: pass}, callback => {
        if (callback == "success"){
            $("#message").html("<div class='alert alert-success' role='alert'>Cahier Python créé avec succès.</div>");
            clearCreateNotebook();
            // Load the notebook list to the select
            socket.emit("load_notebook_list");
        } else {
            $("#message_add_notebook").html("<div class='alert alert-danger' role='alert'>Une erreur est survenue lors de la création du cahier Python.</div>");
        }
    })
}

function clearCreateNotebook(){
    $("#notebook_title").val("");
    $("#notebook_instructions").summernote('code', '');
    $("#notebook_file").val("");
    $("#doc_url").val("");
    $("#doc_file").val("");
    $("#notebook_pass").val("");
    $("#message_add_notebook").html("");
    $("#notebook_title").removeClass("is-invalid");
    $("#notebook_instructions").removeClass("is-invalid");
    $("#notebook_file").removeClass("is-invalid");
    $("#doc_url").removeClass("is-invalid");
    $("#doc_file").removeClass("is-invalid");
    $("notebook_pass").removeClass("is-invalid");
    notebookModal.hide();
}