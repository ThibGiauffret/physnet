// This file contains all the functions related to the quiz and its behaviour in the add/modify classroom modals in the admin panel. It is the connector between the quiz activity and the classroom in the admin panel

$(document).ready(function () {

    socket.emit('load_quiz_list');

    socket.on('load_quiz_list', (data) => {

        if (data == "error") {
            $("#message").html("<div class='alert alert-danger' role='alert'>Une erreur est survenue lors du chargement des boîtes de dépôt.</div>");
            return;
        }

        // For each quiz list
        $(".quiz_list").each(function () {

            // Get the selected value
            let selected = $(this).val();
            // Clean the select
            $(this).html("");
            // check if file_deposit_list has class old
            if (!$(this).hasClass("old")) {
                $(this).html("<option disabled selected value> Choisir un quiz </option>");
                for (var j = 0; j < data.length; j++) {
                    var option = document.createElement("option");
                    option.value = data[j].id;
                    option.text = "Quiz " + data[j].id + " : " + data[j].title;
                    $(this).append(option);
                }
                // Set back the selected value
                $(this).val(selected);
            } else {
                for (let i = 0; i < data.length; i++) {
                    // Add and set the selected quiz
                    if ($(this).attr("data-select") == data[i].id) {
                        $(this).append(`<option value="${data[i].id}" selected>Quiz ` + data[i].id + ` : ` + data[i].title + `</option>`);
                        continue;
                    } else {
                        $(this).append(`<option value="${data[i].id}">Quiz ` + data[i].id + ` : ` + data[i].title + `</option>`);
                    }
                }
            }
        })
    })

})

function addQuizActivityButton(location) {
    let counter;
    if (location == "modifyDocument") {
        counter = doc_modify_count + 1;
        doc_modify_count++;
    } else {
        counter = doc_count + 1;
        doc_count++;
    }
    // add a title input to the addDocument div
    var quiz = document.createElement("div");
    quiz.classList.add("accordion-item");
    quiz.innerHTML = `
        <h2 class="accordion-header" id="panel_${counter}">
        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panel_collapse_${counter}" aria-expanded="true" aria-controls="panelsStayOpen">
        Quiz ${counter}

        </button>
        </h2>

        <div id="panel_collapse_${counter}" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
      <div class="accordion-body">

      <input type="hidden" class="form-control doc_type" value="quiz">

        <div class="input-group mb-3">

        <div class="input-group-prepend">
            <label class="input-group-text" for="classroom_list_modify"><i
                    class="fa-solid fa-circle-question fa-fw"></i></label>
        </div>
        <select class="custom-select quiz_list"></select>
        
         </div>
         </div>
         </div>
         </div>
            
        `;
    $("#" + location).append(quiz);

    socket.emit("load_quiz_list");

}

function addQuizToClassroom(loc, id) {
    console.log("Adding quiz to classroom from " + loc);
    let activity = [];
    let errors = [];

    let select = $('#' + loc + ' #' + id + ' .quiz_list')

    if (select.val() == null) {
        errors.push("un quiz doit être sélectionné");
        select.addClass("is-invalid");
    }

    // Determine if the quiz is an update or a new quiz
    let update;
    if (select.hasClass("old")) {
        update = true;
    } else {
        update = false;
    }

    // Create a document object
    let doc = {
        update: update,
        type: select.parent().parent().find(".doc_type").val(),
        doc_id: parseInt(select.parent().parent().find(".quiz_id").val()),
        id: select.val(),
        text: select.parent().parent().find(".quiz_list option:selected").text().split(": ")[1],
    }
    activity.push(doc);

    // Return the quiz_list and errors
    return {
        activity: activity,
        errors: errors,
    }
}

function loadQuizActivity(location = "modifyDocument", data) {

    doc_modify_count++;

    // Create the quiz
    quiz_id = data.url.split("id=")[1];
    var quiz = document.createElement("div");
    quiz.classList.add("accordion-item");
    quiz.innerHTML = `
    <h2 class="accordion-header" id="panel_${doc_modify_count}">
    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panel_collapse_${doc_modify_count}" aria-expanded="true" aria-controls="panelsStayOpen">
    Quiz ${doc_modify_count}

    </button>
    </h2>

    <div id="panel_collapse_${doc_modify_count}" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
  <div class="accordion-body">

  <input type="hidden" class="quiz_id" value="${data.id}">

  <input type="hidden" class="form-control doc_type" value="quiz">

    <div class="input-group mb-3">

    <div class="input-group-prepend">
        <label class="input-group-text" for="classroom_list_modify"><i
                class="fa-solid fa-circle-question fa-fw"></i></label>
    </div>
    <select class="custom-select quiz_list old" data-select="${quiz_id}">
    </select>
     </div>
     </div>
     </div>
     </div>
        
    `;
    $("#" + location).append(quiz);

    socket.emit("load_quiz_list");
}